
#include <PatriotLight.h>
#include <IoT.h>
#define BLYNK_PRINT Serial  // Set serial output for debug prints
//#define BLYNK_DEBUG       // Uncomment this to see detailed prints
#include <blynk.h>
char auth[] = "Your Auth Key Here";
IoT *iot;

// We're going to start by declaring which pins everything is plugged into.

int led = D7; // This is where your LED is plugged in. The other side goes to a resistor connected to GND.

int photoresistor = A0; // This is where your photoresistor is plugged in. The other side goes to the "power" pin (below).

int power = A5; // This is the other end of your photoresistor. The other side is plugged into the "photoresistor" pin (above).
// The reason we have plugged one side into an analog pin instead of to "power" is because we want a very steady voltage to be sent to the photoresistor.
// That way, when we read the value from the other side of the photoresistor, we can accurately calculate a voltage drop.

int analogvalue; // Here we are declaring the integer variable analogvalue, which we will use later to store the value of the photoresistor.

int manualMode = 0;
int changebuffer  = 0;
int buffersize = 10;
int threshold = 3300;

// Next we go into the setup function.

void setup() {

    iot = IoT::getInstance();
    iot->begin();

 //Light *light1 = new Light(D7, "GARDEN", false, true);
 // Tell IoT about the devices you defined above
 
 Light *light1 = new Light(D7, "GARDEN", false, true);
 iot->addDevice(light1);
 iot->addBehavior(new Behavior(light1, "WATER", '>', 0, 100));      // On
 iot->addBehavior(new Behavior(light1, "WATER", '=', 0, 0));        // Off
    
    // First, declare all of our pins. This lets our device know which ones will be used for outputting voltage, and which ones will read incoming voltage.
    pinMode(led,OUTPUT); // Our LED pin is output (lighting up the LED)
    pinMode(photoresistor,INPUT);  // Our photoresistor pin is input (reading the photoresistor)
    pinMode(power,OUTPUT); // The pin powering the photoresistor is output (sending out consistent power)

    // Next, write the power of the photoresistor to be the maximum possible, so that we can use this for power.
    digitalWrite(power,HIGH);

    // We are going to declare a Particle.variable() here so that we can access the value of the photoresistor from the cloud.
    Particle.variable("analogvalue", &analogvalue, INT);
    Particle.variable("manualMode", &manualMode, INT);
    Particle.variable("threshold", &threshold, INT);
    // This is saying that when we ask the cloud for "analogvalue", this will reference the variable analogvalue in this app, which is an integer variable.

    // We are also going to declare a Particle.function so that we can turn the LED on and off from the cloud.
    Particle.function("led",ledToggle);
    
    // This is saying that when we ask the cloud for the function "led", it will employ the function ledToggle() from this app.

    Serial.begin(9600);
    delay(5000); // Allow board to settle

    Blynk.begin(auth);
}


// Next is the loop function...

void loop() {

    // check to see what the value of the photoresistor is and store it in the int variable analogvalue
    analogvalue = analogRead(photoresistor);

    if (manualMode == 1) {
        if (analogvalue <= threshold) {
            if (changebuffer <= (-1*buffersize)) {
                digitalWrite(led,HIGH);
                changebuffer = 0;
            } else {
                changebuffer -= 1;
            }
        }
        else if (analogvalue > threshold) {
            if (changebuffer > buffersize) {
                digitalWrite(led,LOW);
                changebuffer = 0;
            } else {
                changebuffer += 1;
            }
        }
    }
    delay(10);
    
    iot->loop();

    Blynk.run();
}


BLYNK_WRITE(V2) {
    int r = param[0].asInt();
    int g = param[1].asInt();
    int b = param[2].asInt();
    if (r > 0 || g > 0 || b > 0) {
        RGB.control(true);
        RGB.color(r, g, b);
    } else {
        RGB.control(false);
    }
 
  
 
}
// Finally, we will write out our ledToggle function, which is referenced by the Particle.function() called "led"

int ledToggle(String command) {

  if (command=="automatic") {
      manualMode = 1;
      //digitalWrite(led,HIGH);
      return 1;
    }
    else if (command=="on") {
        manualMode = 2;
        digitalWrite(led,HIGH);
        return 2;
    }
  else if (command=="off") {
       manualMode = 0;
       digitalWrite(led,LOW);
       return 0;
   }
   else {
       return -1;
   }
}
